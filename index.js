const express = require('express');
const app = express();
const cors = require('cors');
const config = require('config');
const morgan = require('morgan');
const mongoose = require('mongoose');
const PORT = config.get('port') || 8080;
const mongoUri = config.get('mongoUri');
const authRouter = require('./routers/AuthRouter');
const userRouter = require('./routers/UserRouter');
const truckRouter = require('./routers/TruckRouter');
const loadRouter = require('./routers/LoadRouter');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);

const start = async () => {
  try {
    await mongoose.connect(mongoUri);

    app.listen(PORT, () => {
      console.log(`Server is starting on port ${PORT}...`);
    });
  } catch (e) {
    console.log(`server error ${e.message}`);
    process.exit(1);
  }
};

start();
